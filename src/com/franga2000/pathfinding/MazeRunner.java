package com.franga2000.pathfinding;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Scanner;

import javax.imageio.ImageIO;

public class MazeRunner {
	static BufferedImage slika;
	static int[][] labirint;
	static P start = null;
	static P cilj = null;

	public static void main(String[] args) throws IOException {
		String filename;
		if (args.length > 0) {
			filename = args[0];
		} else {
			System.out.println("Ime datoteke: ");
			Scanner in = new Scanner(System.in);
			filename = in.nextLine();
			in.close();
		}

		/* Prebere sliko in jo vpiše v array */
		slika = ImageIO.read(new File(filename));
		slika = Util.fixImageBecauseJavaIsStupid(slika);
		labirint = pretvoriSliko(slika);
		System.out.println("Slika prebrana");

		/* Najde začetek in konec */
		for (int y = 0; y < slika.getWidth(); y++)
			for (int x = 0; x < slika.getHeight(); x++)
				if (labirint[y][x] == Color.BLACK.getRGB() || labirint[y][x] == Color.WHITE.getRGB())
					continue;
				else if (labirint[y][x] == Color.GREEN.getRGB())
					start = new P(x, y);
				else if (labirint[y][x] == Color.RED.getRGB())
					cilj = new P(x, y);
				else
					System.out.printf("Neznana barva na (%d,%d) = %s (%s)%n", x, y, labirint[y][x], new Color(labirint[y][x]));

		System.out.printf("Začetek: (%d,%d)%n", start.x, start.y);
		System.out.printf("Konec: (%d,%d)%n", cilj.x, cilj.y);

		Instant startTime = Instant.now();
		List<P> a_star = Pathfinding.A_Star(labirint, start, cilj);
		System.out.println("A-Star done!");
		
		Duration duration = Duration.between(startTime, Instant.now());
		System.out.println("Duartion: " + duration.toMillis() + "ms");
		if (a_star == null)
			System.out.println("A-Star found no paths!");
		else {
			String outfile = Util.suffix(filename, "-a_star");
			writePath(a_star, outfile);
			System.out.println("Written file: " + outfile);
		}
	}

	static void writePath(List<P> path, String filename) throws IOException {
		BufferedImage solved = Util.deepCopy(slika);
		for (P p : path) {
			if (!p.equals(start) && !p.equals(cilj))
				solved.setRGB(p.x, p.y, Color.CYAN.getRGB());
		}
		ImageIO.write(solved, Util.ext(filename), new File(filename));
	}

	static int[][] pretvoriSliko(BufferedImage img) {
		int širina = img.getWidth();
		int višina = img.getHeight();
		int[][] arr = new int[višina][širina];

		for (int y = 0; y < višina; y++)
			for (int x = 0; x < širina; x++)
				arr[y][x] = img.getRGB(x, y);

		return arr;
	}

}
