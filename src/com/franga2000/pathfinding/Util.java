package com.franga2000.pathfinding;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;

/**
 * @author franga2000
 *
 */
public class Util {
	public static boolean DEBUG = false;
	
	/**
	 * Naredi identično kopijo slike
	 */
	public static BufferedImage deepCopy(BufferedImage bi) {
		 ColorModel cm = bi.getColorModel();
		 boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		 WritableRaster raster = bi.copyData(null);
		 return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	}
	
	public static void debug(Object str) {
		if (DEBUG)
			System.out.println(str);
	}
	
	/**
	 * Returns the file extension
	 */
	public static String ext(String filename) {
		return filename.substring(filename.lastIndexOf('.') + 1);
	}
	
	/**
	 * Returns the filename without the extension
	 */
	public static String noext(String filename) {
		return filename.substring(0, filename.lastIndexOf('.'));
	}
	
	/**
	 * Adds some text before the file extension 
	 */
	public static String suffix(String filename, String suffix) {
		return Util.noext(filename) + suffix + "." + Util.ext(filename);
	}
	
	/**
	 * Java sometimes does weird things to images. This tries to maybe fix some of them.
	 * @param i - Probably broken image
	 * @return Possibly mostly fixed image 
	 */
	public static BufferedImage fixImageBecauseJavaIsStupid(BufferedImage i) {
	    BufferedImage rgb = new BufferedImage(i.getWidth(null), i.getHeight(null), BufferedImage.TYPE_INT_RGB);
	    rgb.createGraphics().drawImage(i, 0, 0, null);
	    return rgb;
	}
	
}
