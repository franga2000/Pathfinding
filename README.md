A half-assed attempt at A*
===
This is my half-assed attempt at implementing the A* (A-star) pathfinding algorithm in Java.
It is absolutely terrible, but does actually *work*.

## What?
The A* algorithm is an extension of Dijkstra's pathfinding algorithm that has better performance, because it uses a heuristic (in this case the euclidean distance between points) to first consider points that are more likely to be correct.

## Really?
Yes. Try it out:

 - Draw a maze (1px brush recommended) with black pixels as walls and white pixels as paths
 - Draw a green (0, 255, 0) pixel for the start point
 - Draw a red (255, 0, 0) pixel for the end point
 - Save it as a lossless image file (PNG recommended). No JPEG!
 - Run the `MazeRunner` class (or the JAR package)
 - Type in the path to your maze (or pass it as a command line argument)

If you're lucky, you should have a new file with a cyan path drawn between the points.  
~~If your maze was really big, you'll also have some `progress-` files for every 1000 iterations. Just delete them.~~ I'm too lazy to change the code.

## Why?
I needed something challenging to test the [Ballmer peak](https://xkcd.com/323/).

## How?
I'm too lazy to explain. Here's some good explanations:

 - Computerphile video: https://www.youtube.com/watch?v=ySN5Wnu88nE
 - Cool game dev blog: http://www.redblobgames.com/pathfinding/a-star/introduction.html
 - What I used: http://www.policyalmanac.org/games/aStarTutorial.htm

## License?
If you insist: http://www.wtfpl.net/about/
