package com.franga2000.pathfinding;

/**
 * Opis točke v prostoru. <b>OPIS, ne reprezentacija</b> <br>
 * točki, ki se ujemata po koordinatah sta si vedno enaki (==) tudi, če se razlikujeta v drugih lastnostih.
 */
public class P {
	public final int x;
	public final int y;
	
	/**
	 * Prejšnja točka
	 */
	public P parent;
	
	/**
	 * H vrednost - "kvaliteta" točke. V tem primeru standardna razdalja do cilja
	 */
	public int h;
	
	/**
	 * G vrednost - število korakov od zašetka do te točke
	 */
	public int g = 1;
	
	/**
	 * F vrednost - glavni kriterij za primerjavo. <br>
	 * Izračunan po <code>f = g + h</code>
	 */
	public int f = Integer.MAX_VALUE;
	
	/**
	 * Vpiše novo G vrednost in ponovno izračuna F
	 * @param g
	 */
	public void updateG(int g) {
		this.g = g;
		this.f = this.g + this.h;
	}
	
	public P(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * točki sta si enaki, če se ujemata v koordinatah. Ostali podatki se ne preverjajo!
	 */
	public boolean equals(Object other) {
		P p = (P) other;
		return this.x == p.x && 
				this.y == p.y;
	}
	
	/**
	 * točka z nižjo F vrednostjo je boljša
	 */
	public int compareTo(P other) {
		if (this.equals(other))
			return 0;
		return this.f > other.f ? +1 : this.f < other.f ? -1 : 0;
	}
	
	@Override
	public int hashCode() {
		// TODO: Preveri, če je to res potrebno
		return (int) (Math.pow(Math.max(this.x, this.y), 2) + Math.max(this.y, this.y*2 - x));
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ", g=" + g + ", f=" + f + ")@" + hashCode() + (parent != null ? " with parent " + parent : "");
	}
}
