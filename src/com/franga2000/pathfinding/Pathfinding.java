package com.franga2000.pathfinding;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Pathfinding {

	/**
	 * Pretvori sliko v int[y][x] z barvami v RGB formatu
	 * 
	 * @param img - Vhodna slika
	 * @return int[y][x] z barvami v RGB formatu
	 */
	static int[][] pretvoriSliko(BufferedImage img) {
		int širina = img.getWidth();
		int višina = img.getHeight();
		int[][] arr = new int[višina][širina];

		for (int y = 0; y < višina; y++)
			for (int x = 0; x < širina; x++)
				arr[y][x] = img.getRGB(x, y);

		return arr;
	}

	/**
	 * Izračuna približno razdaljo med dvema točkama
	 * 
	 * @param p1 - Prva točka
	 * @param p2 - Druga točka
	 * @return Razdaljo med točkama, zaokroženo na celo število
	 */
	static int eucld(P p1, P p2) {
		int dx = Math.abs(p1.x - p2.x);
		int dy = Math.abs(p1.y - p2.y);
		double d = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
		return (int) Math.round(d);
	}

	/**
	 * Preveri, če dana točka obstaja
	 */
	static boolean obstaja(int[][] map, P p) {
		if (p.y >= map.length || p.y < 0)
			return false;
		if (p.x >= map[p.y].length || p.x < 0)
			return false;
		return true;
	}

	/**
	 * Preveri, če je točka zid
	 * 
	 * @return true če je točka črna, false če ni
	 */
	static boolean zid(int[][] map, P p) {
		if (!obstaja(map, p))
			return true;
		return map[p.y][p.x] == Color.BLACK.getRGB();
	}

	/**
	 * Vrne seznam sosednjih točk, na katere se je močno premakniti
	 * 
	 * @return Seznam točk, ki se dane točke dotikajo in niso zid
	 */
	static List<P> sosedi(int[][] map, P p) {
		List<P> sosedi = new ArrayList<P>();
		if (!zid(map, new P(p.x + 1, p.y))) // Desno
			sosedi.add(new P(p.x + 1, p.y));
		if (!zid(map, new P(p.x - 1, p.y))) // Levo
			sosedi.add(new P(p.x - 1, p.y));
		if (!zid(map, new P(p.x, p.y + 1))) // Gori
			sosedi.add(new P(p.x, p.y + 1));
		if (!zid(map, new P(p.x, p.y - 1))) // Doli
			sosedi.add(new P(p.x, p.y - 1));
		// TODO: Option for diagnonals, maybe
		return sosedi;
	}

	/**
	 * Za dano točko rekonstruira pot do nje
	 * 
	 * @return Seznam predhodnjih toč v obratnem vrstnem redu (cilj->začetek)
	 */
	static List<P> reconstruct(P cilj) {
		List<P> path = new ArrayList<P>();
		path.add(cilj);
		P p = cilj;
		while (p.parent != null) {
			path.add(p.parent);
			p = p.parent;
		}
		return path;
	}

	/**
	 * Ne pretirano dobra implementacija A* algoritma
	 * 
	 * @param start - Začetna točka
	 * @param cilj - Končna točka
	 * @return Optimalna pot od začetka do konca
	 */
	static List<P> A_Star(int[][] map, P start, P cilj) {
		int i = 0;
		List<P> open = new LinkedList<P>();
		List<P> closed = new LinkedList<P>();

		open.add(start);
		while (open.size() > 0) {
			Util.debug("open: " + open.get(0));
			P current = open.get(0);
			Util.debug("considering " + current);

			if (current.equals(cilj)) {
				System.out.println("A-Star returned in " + i + " cycles");
				return reconstruct(current);
			}

			closed.add(current);
			open.remove(current);

			for (P p : sosedi(map, current)) {
				if (closed.contains(p)) {
					Util.debug("    " + p + " is closed");
					continue;
				}

				if (!open.contains(p)) {
					Util.debug("    " + p + " is not open");
					p.parent = current;
					p.h = eucld(p, cilj);
					p.updateG(current.g + 1);
					open.add(p);
					open.sort((p1, p2) -> p1.compareTo(p2));
				} else {
					Util.debug("    " + current + " is open");
					if (p.g > current.g) {
						Util.debug("    " + current + " has better g");
						p.parent = current;
						p.updateG(current.g + 1);
					}
				}
				i++;
			}
		}
		return null;
	}
}
